<?php

class Controller_Script extends Controller_Template
{

	public function action_index()
	{
		$data["subnav"] = array('index'=> 'active' );
		$this->template->title = 'Script &raquo; Index';
		$this->template->content = View::forge('script/index', $data);
	}

	public function action_ohira()
	{
		return View::forge('script/ohira');
	}
}
