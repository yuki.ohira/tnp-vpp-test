<?php

use Fuel\Core\Input;
use Fuel\Core\Validation;

class Controller_Test extends Controller_Rest
{
    public $id;



    protected $rest_format = 'json';
//    protected $rest_format = 'xml';

    /**
     * APIでサポートするフォーマットの規定
     * コメントアウトされていないフォーマット
     * のみが対象となる。
     *
     * @var array
     */
    protected $_supported_formats = [
//            'xml'        => 'application/xml',
//              'rawxml'     => 'application/xml',
        'json' => 'application/json',
//                'jsonp' => 'text/javascript',
//              'serialized' => 'application/vnd.php.serialized',
//              'php'        => 'text/plain',
//              'html'       => 'text/html',
//              'csv'        => 'application/csv',
    ];

    public function before()
    {
        parent::before();

//        $uri = Uri::string();

//        $uri = htmlspecialchars($_SERVER['REQUEST_URI'], ENT_QUOTES, 'UTF-8');

        $url = Uri::segments();

        print_r($url);


         $this->id = Input::param('Service_Provision_Function_Key');

//         var_dump( $this->id );

        if(($this->id == null) && (strlen($this->id) ==0)) {
            $message = sprintf("%d %s", 400, "Bad Request Test");
            header(sprintf("HTTP/1.1 %s", $message));
            exit;
        }

    }

    public function get_read()
    {
        // $data["subnav"] = array('read'=> 'active' );
        // $this->template->title = 'Test &raquo; Read';
        // $this->template->content = View::forge('test/read', $data);
        $customer_response = array();
//        $id = Input::param('service_provision_function_key');
        $all = Input::param('all');

 //       var_dump($this->id);
//        var_dump($all);
        $val_array = array('service_provision_function_key' => $this->id, 'all' => $all);
        $validation = null;
        $val = Validation::forge();
//        $val->add_callable('Validation_MyValidation');
        if($this->id != null) {
            $val->add_field('service_provision_function_key', 'サービスコネクションID', 'trim|min_length[19]');
        }
        if($all != null) {
            $val->add_field('all', '全件指定', 'trim|min_length[4]|max_length[5]');
        }
        if(($all != null && strlen($all) >0) && ($this->id != null && strlen($this->id) >0)) {
            $message = sprintf("%d %s", 400, "Bad Request Test");
            header(sprintf("HTTP/1.1 %s", $message));
            exit;
        }

        $validation = $val->run($val_array);

        if (!$validation) {
            $errors = $val->error();
            foreach ($errors as $key => $value) {
//                echo $value->get_message();
            }
            $message = sprintf("%d %s", 400, "Bad Request");
            header(sprintf("HTTP/1.1 %s", $message));
            exit;
        }


        if($all != null){
            if($all = 'true'){
                $sql = 'select * from tbl_sample_infos ';
                $query = \DB::query($sql);
                $result = $query->execute();
                $on_key = $result->as_array('service_provision_function_key');
                foreach ($on_key as $id => $item) {
                    $line = array(
                        'user_id' => $item['service_provision_function_key'],
                        'dead_band_width' => (double)$item['dead_band_width']
                    );
                    $customer_response[] = $line;
                    unset($line);
                }
                $res = array(
                    'response' => array(
                        'response_dt' => date(DateTime::ISO8601),
                        'customer_response' => $customer_response,
                    )
                );
                return $this->response($res, 200);
                exit;
            }
        }



        $params = array('min_id' => $this->id);
        $sql = 'select * from tbl_sample_infos WHERE service_provision_function_key = :min_id ';
        $query = \DB::query($sql);
        $query->parameters($params);
        $result = $query->execute();
        $on_key = $result->as_array('service_provision_function_key');
        foreach ($on_key as $id => $item) {
            $line = array(
                'user_id' => $item['service_provision_function_key'],
                'dead_band_width' => (double)$item['dead_band_width']
            );
            $customer_response[] = $line;
            unset($line);
        }
        $res = array(
            'response' => array(
                'response_dt' => date(DateTime::ISO8601),
                'customer_response' => $customer_response,
            )
        );
        return $this->response($res, 200);
    }

}
