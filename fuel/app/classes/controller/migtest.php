<?php

class Controller_Migtest extends Controller_Rest
{
    protected $rest_format = 'json';
//    protected $rest_format = 'xml';

    /**
     * APIでサポートするフォーマットの規定
     * コメントアウトされていないフォーマット
     * のみが対象となる。
     *
     * @var array
     */
    protected $_supported_formats = [
//            'xml'        => 'application/xml',
//              'rawxml'     => 'application/xml',
//        'json' => 'application/json',
//                'jsonp' => 'text/javascript',
//              'serialized' => 'application/vnd.php.serialized',
//              'php'        => 'text/plain',
        'html'       => 'text/html',
//              'csv'        => 'application/csv',
    ];

    public function get_test()
    {
 //       $id = Input::param('id');
//        $message = Input::param('message');

        $db = Database_Connection::instance();
        $db->start_transaction();

        try{
            $array = array(1,2,3,4,5,6,7,8,9,10);
            for($i = 0; $i < count($array);$i++){
                if($array[$i]!=4){
                    if($array[$i] <= 5){
                        $sql = "INSERT INTO migtest VALUES ('$array[$i]' , 'smallNum')";
                        $query = \DB::query($sql);
                        $query->execute();
                    }else{
                        $sql = "INSERT INTO migtest VALUES ('$array[$i]' , 'bigNum')";
                        $query = \DB::query($sql);
                        $query->execute();
                    }
                }else{
                    //throw new Exception();
                }
            }
            $db->commit_transaction();
            return $this->response(200);
        }catch(Exception $ex){
            $db->rollback_transaction();
            \Log::write("ERROR",print_r($ex, true));
            return $this->response(400);
        }
	}
}
