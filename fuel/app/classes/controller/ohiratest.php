<?php

class Controller_Ohiratest extends Controller_Rest
{
	public $param1;

	public function before()
	{
		parent::before();
		$this->param1 = Input::get('ZipCode');
		$param = $this->param1;
		$param_arr = array('ZipCode'=>$param);		

		$val = Validation::forge();
		
		$val->add_field('ZipCode','Your zipcode','required');
		
		if($val->run($param_arr)){
			Log::info('validation成功');
		}else{
			Log::info('validation失敗');
	            	$errors = $val->error();
			$error_msg = '';
        	    	foreach ($errors as $key => $value) {
              	 		$error_msg = $error_msg.$value->get_message();
            		}
            		$message = sprintf("%d %s", 400, "Bad Request");
            		header(sprintf("HTTP/1.1 %s", $message));
			Log::info($error_msg);
			exit;
		}
	}


	public function get_action1()
	{
		try{
			Log::info('ohiratest');
			$param = $this->param1;
			$url = "http://zipcloud.ibsnet.co.jp/api/search";
			$request = \Request::forge($url, 'curl');
			$request->set_method('get');
			$request->set_params(array('zipcode'=>$param));
			$request->execute();
			$response = $request->response();
		
			$arr= json_decode($response,true);	
			$address1= $arr['results'][0]['address1'];
			$address2= $arr['results'][0]['address2'];
			return $this->response("<p>郵便番号=".$param."は".$address1."の".$address2."です。</p>",200);	
		}catch(Exception $e){
			return $e->getMessage();
		}
	}

}
