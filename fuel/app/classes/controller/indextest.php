<?php

class Controller_Indextest extends Controller_Template
{

	public function action_index()
	{
		$data["subnav"] = array('index'=> 'active' );
		$this->template->title = 'Indextest &raquo; Index';
		$this->template->content = View::forge('indextest/index', $data);
	}

	public function action_test()
	{
		$data["subnav"] = array('index'=> 'active' );
		$this->template->title = 'Indextest &raquo; Index';
		$this->template->content = View::forge('indextest/index', $data);
	}
}
