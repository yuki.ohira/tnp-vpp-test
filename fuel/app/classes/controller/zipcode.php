<?php

class Controller_Zipcode extends Controller_Rest
{
    protected $rest_format = 'json';
//    protected $rest_format = 'xml';

    /**
     * APIでサポートするフォーマットの規定
     * コメントアウトされていないフォーマット
     * のみが対象となる。
     *
     * @var array
     */
    protected $_supported_formats = [
//            'xml'        => 'application/xml',
//              'rawxml'     => 'application/xml',
//        'json' => 'application/json',
//                'jsonp' => 'text/javascript',
//              'serialized' => 'application/vnd.php.serialized',
//              'php'        => 'text/plain',
             'html'       => 'text/html',
//              'csv'        => 'application/csv',
    ];

	public function get_regist()
	{
	    $param = Input::get('zipcode');
        $val_array = array('zipcode' => $param);
        $validation = null;
        $val = Validation::forge();

            $val->add('zipcode', '郵便番号')
                ->add_rule('required')
                ->add_rule('trim')
                ->add_rule('exact_length',7)
                ->add_rule('valid_string', array('numeric','dashes','utf8'), '数字');

        $validation = $val->run($val_array);

        if (!$validation) {
            $errors = $val->error();
            foreach ($errors as $key => $value) {
               echo $value->get_message();
            }
            $message = sprintf("%d %s", 400, "Bad Request");
            header(sprintf("HTTP/1.1 %s", $message));
            exit;
        }


	    $url = "http://zipcloud.ibsnet.co.jp/api/search";
        $request  = \Request::forge($url, 'curl');
        $request->set_method('get');
        $request->set_params(array('zipcode'=> $param));
        $request->execute();
        $response = $request->response();

        $arr= json_decode($response,true);

        $Zipcode = $arr['results'][0]['zipcode'];
        $prefcode = $arr['results'][0]['prefcode'];
        $address1 = $arr['results'][0]['address1'];
        $address2 = $arr['results'][0]['address2'];
        $address3 = $arr['results'][0]['address3'];
        $kana1 = $arr['results'][0]['kana1'];
        $kana2 = $arr['results'][0]['kana2'];
        $kana3 = $arr['results'][0]['kana3'];
        $status = $arr['status'];
        $result = $arr['results'];
        $message = $arr['message'];


        if($status != '200' || $result == null && strlen($result) ==0){
            return $this->response("<p>登録に失敗しました</p><p>$message</p>", $status, $message);
            exit;
        }

        $select_sql = "SELECT * from zipcode_regist WHERE address3 = '%s'";
        $sql = sprintf($select_sql,$address3);
        $query = \DB::query($sql);
        $select_result = $query->execute();

        if(count($select_result)==0){
            $sql = "INSERT INTO zipcode_regist VALUES ('$Zipcode','$prefcode','$address1','$address2','$address3','$kana1','$kana2','$kana3')";
            $query = \DB::query($sql);
            $query->execute();
            return $this->response("<p>登録が完了しました</p>");
            exit;
        }else{
            return $this->response("<p>既に登録済みです</p>");
            exit;
        }
	}
}
