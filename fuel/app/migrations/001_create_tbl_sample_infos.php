<?php

namespace Fuel\Migrations;

class Create_tbl_sample_infos
{
	public function up()
	{
		\DBUtil::create_table('tbl_sample_infos', array(
			'id' => array('type' => 'SERIAL'),
			'service_provision_function_key' => array('constraint' =>19, 'type' => 'VARCHAR'),
			'dead_band_width' => array('type' => 'double precision'),
			'created_at' => array('type' => 'timestamp with time zone'),
			'updated_at' => array('type' => 'timestamp with time zone'),
		), array('id'));

		\DBUtil::create_index('tbl_sample_infos', 'service_provision_function_key', 'UNIQUE');
	}

	public function down()
	{
		\DBUtil::drop_table('tbl_sample_infos');
	}
}
