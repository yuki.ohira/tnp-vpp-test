<?php

namespace Fuel\Migrations;

class Create_migtest
{
	public function up()
	{
		\DB::query('create table migtest (id integer not null,message varchar(30))')->execute();
	}

	public function down()
	{
		\DBUtil::drop_table('migtest');
	}
}
