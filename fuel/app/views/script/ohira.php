<!DOCTYPE html>
<html>
<head>
<title>テスト</title>
</head>
<body>
<script type = "text/javascript">
	window.onload = function(){
		document.getElementById("jsTrue").onmouseover = function(){
			document.getElementById("jsTrue").innerText = 'マウスオーバー中';
		}
		document.getElementById("jsTrue").onmouseout = function(){
			document.getElementById("jsTrue").innerText = 'JavaScript使用';
		}
		document.getElementById("jsTrue").onmousedown = function(){
			document.getElementById("jsTrue").innerText = 'クリック中';
		}
		document.getElementById("jsTrue").onmouseup = function(){
			document.getElementById("jsTrue").innerText = 'JavaScript使用';
		}
		
		//時刻表示	
		setInterval(function(){
			var dd = new Date();
			document.getElementById("T1").innerHTML= dd.toLocaleString();
			}, 1001);

		document.getElementById("scroll").style.height = "100px";
		//document.getElementById("aaaa").style.backgroundImage = "https://www.google.com/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png";
		//console.log(document.getElementById("aaaa"));
		
	}

	//さいころ
	function saikoro(){
	var r = Math.floor(Math.random() * 10) +1;
	document.getElementById("sai").innerHTML = r;
	}

	//ブラウザ表示
	function checkAge(){
		result = confirm("あなたはGoogle派ですか？");
		if (result) {
		wURL = "https://google.com/";//OKならこのURLを取得
	} else {
		wURL = "http://www.yahoo.co.jp/";//キャンセルならこのURLを取得
	}
	newWin = window.open(); //新規ウィンドウを開く
	newWin.location.href = wURL; //取得したURLを表示
	}

	
	function omikuji(){
		var rand = Math.floor(Math.random() * 100);
		var msg = "大吉";
		if(rand > 9) var msg = "中吉";
		if(rand > 29) var msg = "吉";
		if(rand > 69) var msg = "凶";
		if(rand > 89) var msg = "大凶";
		document.getelementById("kuji").innerHTML = msg;
		alert(msg);
	}
	
	//イメージ切り替え
	icon = new Array();
	
	for (i = 0; i < 3 ; i++){
		icon[i] = new Array();
		icon[i][0] = new Image();
		icon[i][1] = new Image();
	}


	icon[0][0].src = "go.gif";
	icon[0][1].src = "enter.gif";
	icon[1][0].src = "ice_pink.gif";
	icon[1][1].src = "ice_green.gif";	
	icon[2][0].src = "ice_white.gif";	
	icon[2][1].src = "ice_blue.gif";

	function msg2(num,onoff){
		document.getElementById("image"+num).src = icon[num][onoff].src;
	}
	
	//画像スクロール表示
	//scrollBG();
	function scrollBG(){
		target = document.getElementById("scroll");
		target.style.height = "100px";
		document.getElementById("scroll").style.backgroundImage = "url(img/train.jpg)";
		document.getElementById("scroll").style.ckgroundRepeat = "repeat-x";
		
		count = 0;
		timerID = setInterval('scrollBG_countup()', 500);
	}

	function scrollBG_countup(){
		count++;
		if(count > 99) count = 0;
		document.getElementById("scroll").style.backgroundPosition = count + "px 0px";
	}

</script>

<button id="jsTrue">JavaScript使用</button><br/>
<button id="jsFalse">JavaScript不使用</button>
<br/>

<div id = "T1"></div>

<form>
  <input type="button" value="10面サイコロ" onClick="saikoro()">
<span id="sai">-</span>
</form>

<form method="post" onsubmit="return checkAge()">
<input type="submit" value="ブラウザ選択">
</form>

<form>
<input type="button" value ="おみくじ" onClick="omikuji()">
<span id="kuji">-</span>
</form>

<a href = "javascript:;"onMouseOver = "msg2(0,0)" onMouseOut = "msg2(0,1)"><image id="image0" src= "https://illust-stock.com/wp-content/uploads/soft-cream.png" width = "100" height="50" border="0"></a>
<a href = "javascript:;"onMouseOver = "msg2(1,0)" onMouseOut = "msg2(1,1)"><image id="image1" src= "ice_green.gif" width = "32" height=32" border="0"></a>
<a href = "javascript:;"onMouseOver = "msg2(2,0)" onMouseOut = "msg2(2,1)"><image id="image2" src= "ice_choco.gif" width = "32" height="32" border="0"></a>

<p id="scroll">テキスト<span id = 'aaaa' style="background-image:url(https://www.google.com/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png)"></span></p>
</body>
</html>
