<!DOCTYPE html>
<html>
<head>
<title>テスト</title>
</head>
<body>
<script type = "text/javascript">
	var text;
        var textCurrent;
        var counter;
	window.onload = function(){
		document.getElementById("jsTrue").onmouseover = function(){
			document.getElementById("jsTrue").innerText = 'マウスオーバー中';
		}
		document.getElementById("jsTrue").onmouseout = function(){
			document.getElementById("jsTrue").innerText = 'JavaScript使用';
		}
		document.getElementById("jsTrue").onmousedown = function(){
			document.getElementById("jsTrue").innerText = 'クリック中';
		}
		document.getElementById("jsTrue").onmouseup = function(){
			document.getElementById("jsTrue").innerText = 'JavaScript使用';
		}
		
//		document.getElementById("scroll") = function(){
//              	document.getElementById("scroll").style.height = "100px";
//            		document.getElementById("scroll").style.backgroundImage = "url('https://www.sozai-library.com/wp-content/uploads/2016/12/9407-300x225.jpg')";
//          		document.getElementById("scroll").style.backgroundRepeat = "repeat-x";

//        		count = 0;
//      		var timerID = setInterval('scrollBG_countup(){	count++;if (count > 99) count = 0;document.getElementById("scroll").style.backgroudPosition = count + "px 0px";	}'
//						,500);
//		}	

		//時刻表示	
//		setInterval(function(){
//			var dd = new Date();
//			document.getElementById("T1").innerHTML= dd.toLocaleString();
//			}, 1000);

		text = document.getElementById("sample").firstChild.nodeValue;
		displayOneByOne();

		autoScroll();
		
		scrollBG();
		scrollBG_countup();

		clock();
	}

	function clock() {
 		   var now = new Date();
 		   document . getElementById( 'clock' ) . innerHTML= now . toLocaleString();
 		   window . setTimeout( "clock()", 1000);
	}

	//さいころ
	function saikoro(){
		var r = Math.floor(Math.random() * 10) +1;
		document.getElementById("sai").innerHTML = r;
	}

	//ブラウザ表示
	function checkAge(){
		result = confirm("あなたはGoogle派ですか？");
		if (result) {
		wURL = "https://google.com/";//OKならこのURLを取得
	} else {
		wURL = "http://www.yahoo.co.jp/";//キャンセルならこのURLを取得
	}
	newWin = window.open(); //新規ウィンドウを開く
	newWin.location.href = wURL; //取得したURLを表示
	}

	//おみくじ	
	function omikuji(){
		var rand = Math.floor(Math.random() * 100);
		var msg = "大吉";
		if(rand > 9)  msg = "中吉";
		if(rand > 29)  msg = "吉";
		if(rand > 69)  msg = "凶";
		if(rand > 89)  msg = "大凶";
		document.getElementById("kuji").innerHTML = msg;
	}


	//フォームの内容を一行にまとめ表に追加
	function appendToTable(){
		var formObject = document.getElementById("form");
		var tableObject = document.getElementById("table");
		var tr = "<tr>";
		tr += "<td>" + formObject.formName.value + "</td>";
		tr += "<td>" + formObject.formArea.value + "</td>";
		tr += "<td>" + formObject.formAge.value + "</td>";
		tr += "<td>" + formObject.formComent.value + "</td>";
		tr += "</tr>";
		tableObject.insertAdjacentHTML("beforeend", tr);
	}
		
	//画像横スクロール
	var count = 0;
	function scrollBG(){
		document.getElementById("scroll").style.height = "200px";
		document.getElementById("scroll").style.backgroundImage = "url('https://www.sozai-library.com/wp-content/uploads/2016/12/9407-300x225.jpg')";
		document.getElementById("scroll").style.backgroundRepeat = "repeat-x";

		count = 0;
		var timerID = setInterval('scrollBG_countup()',500);
	}

	function scrollBG_countup(){
		count++;
//		console.log(count);
		if (count > 99) count = 0;
		document.getElementById("scroll").style.backgroudPosition = count + "px 0px";
	}

	//キー検知
	function keyClick(event){
		var key_event = event || window.event;
		var key_shift = (key_event.shiftKey);
		var key_ctrl = (key_event.ctrlKey);
		var key_alt = (key_event.altKey);
		
		var s = "click!";
		if(key_shift) s += "shiftキー";
		if(key_ctrl) s += "ctrlキー";
		if(key_alt) s += "altキー";

		document.getElementById("text_key").innerHTML = s;
	}

	//テキスト1文字ずつ表示
	function displayOneByOne(){
		textCurrent = document.getElementById("sample").firstChild.nodeValue;
		if(textCurrent.length == text.length){
			document.getElementById("sample").innerHTML = '';
			counter = 0;
		}
		document.getElementById("sample").innerHTML = text.substr(0, ++counter) + '<br/>';
		setTimeout('displayOneByOne()', 500);
	}

	//テキストを自動横スクロール
	var scrollx = 0;
	function autoScroll(){
		var box = document.getElementById("box");
		box.scrollLeft = ++scrollx;
		if(scrollx < box.scrollWidth - box.clientWidth){
			setTimeout("autoScroll()", 20);
		}else{
			scrollx = 0;
			box.scrollLeft = 0;
			setTimeout("autoScroll()",20);
		}
	}	

</script>
スクリプトテスト
<form id="example">
<h1>Gitテスト</h1><br/>
<button id="jsTrue">JavaScript使用</button><br/>
<button id="jsFalse">JavaScript不使用</button>
</form>
<br/>

<div id="clock"></div>

<!-- <div id = "T1"></div><br/> -->
<!-- <div id = "scroll"></div> -->

<form>
  <input type="button" value="10面サイコロ" onClick="saikoro()">
<span id="sai">-</span>
</form>
<br/>

<form method="post" onsubmit="return checkAge()">
<input type="submit" value="ブラウザ選択">
</form>
<br/>

<form>
<input type="button" value ="おみくじ" onClick="omikuji()">
<span id="kuji">-</span>
</form>
<br/>

<form id="example">
<input type="button" value ="スクロール" onClick="scrollBG()">
<div id = "scroll"></div>
</form><br/>

<input type="button" value="クリック" onClick="keyClick(event)">
<span id="text_key"></span>
<br/>

<div id="sample">こんにちは！</div><br/>

<div id="box">
	<p id="text">テスト！自動的に最後までスクロールします。</p>
</div><br/>

<div id="wrapSampleForm">
<form id ="form" >
	【入力欄】
	<br/>
	名前：<input type="text" name="formName" >
	<br/>
	地域：
		<select name="formArea">
			<option selected>-</option>
			<option >関東</option>
			<option>東海</option>
			<option>関西</option>
		</select>
	<br/>
	年齢：
		<select name= "formAge">
			<option selected>-</option>
			<option >10代</option>
			<option>20代</option>
			<option>30代</option>
		</select>
	<br/>
	コメント：<textarea name= "formComent"></textarea>
	<br/>
</form>
<button onclick= "appendToTable()">追加</button>
</div>

<table id="table">
	<tr>
		<th id="tableName">名前</th>
		<th id="tableArea">地域</th>
		<th id="tableAge">年齢</th>
		<th id="tableComent">コメント</th>
	</tr>
</table>
</body>
</html>
<style>
#wrapSampleForm {
    width: 500px;
    margin: 10px;
    padding: 10px;
    border: 1px solid gray;
    border-radius: 10px;
}
#wrapSampleForm {
    background-color: #f0f8ff;
}
#table {
    background-color: #ffffcc;
}
#table th {
    color: #000000 !important;
}
#table th,
#table td {
    font-size: 15px !important;
    border: 1px solid gray;
    background-color: #ffffcc;
}
#tableName {
    width: 20%;
}
#formArea {
    width: 15%;
}
#formAge {
    width: 15%;
}
#tableComent {
    width: 50%;
}
#form * {
    margin: 0 0 7px 0;
    vertical-align: text-top;
}
#example {
	margin-right: 20px;
	margin-left: 20px;
	padding: 10px;
	background-color: #FFFFCC;
	border: thin solid #000000;
	overflow: auto;
	font-size: 14px;
}
#sample {
    width: 300px;
    padding: 30px;
    font-size: 30px;
    background-color: red;
    color: white;
    text-align: left;
    border-radius: 10px;
}
#box {
    width: 300px;
    overflow: hidden;
    background-color: yellow;
    text-align: center;
    line-height: 1.5em;
    font-size: 16px;
    border: 1px solid gray;
    border-radius: 10px;
}
#text {
    white-space: nowrap;
    margin: 0 0 0 300px !important;
    width: 700px;
    text-align: left;
}
#clock {
    font-weight: bold;
    font-size: 25px;
    color: white;
    background: gray no-repeat center top;
    text-align: center;
    width: 500px;
    padding: 40px 0 15px 0;
    border: 2px #000000 solid;
}
</style>
