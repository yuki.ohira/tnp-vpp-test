<?php

namespace Fuel\Tasks;

class Ohiratest
{

	/**
	 * This method gets ran when a valid method name is not used in the command.
	 *
	 * Usage (from command line):
	 *
	 * php oil r ohiratest
	 *
	 * @return string
	 */
	public function run($name)
	{
		//$query = \DB::select()->from('zipcode_regist')->execute();
		//var_dump($query);
		
		for($i = 1;$i <=200000;$i++){
			$ram_str = substr(base_convert(hash('sha256', uniqid()), 16, 36), 0, 100);
			//\DB::insert('index_test')->set(array('key' => $i,'value' => $ram_str))->execute();
		}
		\Log::write("DEBUG",'insert完了');
	}



	/**
	 * This method gets ran when a valid method name is not used in the command.
	 *
	 * Usage (from command line):
	 *
	 * php oil r ohiratest:index "arguments"
	 *
	 * @return string
	 */
	public function index($args = NULL)
	{
		echo "\n===========================================";
		echo "\nRunning task [Ohiratest:Index]";
		echo "\n-------------------------------------------\n\n";

		/***************************
		 Put in TASK DETAILS HERE
		 **************************/
	}

	public function select($args = NULL)
	{
		$now = microtime(true);
		\DB::query("select * from index_test where value='tsuganezawa'")->execute();
		$end_time = microtime(true);
		$dif = $end_time - $now; 
		echo ($dif * 1000)."ミリ秒\n";
	}

}
/* End of file tasks/ohiratest.php */
