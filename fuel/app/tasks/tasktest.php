<?php

namespace Fuel\Tasks;

class Tasktest
{

	/**
	 * This method gets ran when a valid method name is not used in the command.
	 *
	 * Usage (from command line):
	 *
	 * php oil r tasktest
	 *
	 * @return string
	 */
	public function run($args = NULL)
	{
		echo "\n===========================================";
		echo "\nRunning DEFAULT task [Tasktest:Run]";
		echo "\n-------------------------------------------\n\n";

		/***************************
		 Put in TASK DETAILS HERE
		 **************************/

		if(empty($args)){
			\Log::write("ERROR","引数がありません");
		}else{
			\Log::write("DEBUG",$args);
		}
	}



	/**
	 * This method gets ran when a valid method name is not used in the command.
	 *
	 * Usage (from command line):
	 *
	 * php oil r tasktest:index "arguments"
	 *
	 * @return string
	 */
	public function index($args = NULL)
	{	echo "\n===========================================";
		echo "\nRunning task [Tasktest:Index]";
		echo "\n-------------------------------------------\n\n";

		/***************************
		 Put in TASK DETAILS HERE
		 **************************/

	}

}
/* End of file tasks/tasktest.php */
