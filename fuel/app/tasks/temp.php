<?php

namespace Fuel\Tasks;

class Temp
{

	/**
	 * This method gets ran when a valid method name is not used in the command.
	 *
	 * Usage (from command line):
	 *
	 * php oil r servernortification
	 *
	 * @return string
	 */
	public function run($args = NULL)
	{
		echo "\n===========================================";
		echo "\nRunning DEFAULT task [Servernortification:Run]";
		echo "\n-------------------------------------------\n\n";

		/***************************
		 Put in TASK DETAILS HERE
		 **************************/
		$array=
			array(
   				 "text"=> "サーバー落としてね!!",
    				"color"=> "info", // info, warning, danger, success
    				"attachments"=>
    			  		array(
						array(
        						"title"=> "This is sample title",
        						"value"=> "This is sample value"
						)
      		       	,
				array(
        			 "image_url"=> 'https://pbs.twimg.com/profile_images/2332777670/bdow83yd8jy5lh2nyuu0.png'
      				)
    					)
		);

		//$payload =  .json_encode($array, JSON_UNESCAPED_UNICODE|JSON_PRETTY_PRINT);	
		//var_dump($payload);
		//exit();

		$url = "https://hooks.tocaro.im/integrations/inbound_webhook/qngpzb6i3wamm4lpmhafsy2lgewqcigj"; 
		//var_dump($url);
			
		//$request  = \Request::forge($url, 'curl');
		$request = \Request::forge($url, 'curl');
        	$request->set_method('post');
        	//$request->set_mime_type('json');
        	$request->set_header('Content-Type','application/json');
        	$request->set_params($array);
        	$request->execute();
       		 $response = $request->response();
		
		//echo($payload->image_url);
		}



	/**
	 * This method gets ran when a valid method name is not used in the command.
	 *
	 * Usage (from command line):
	 *
	 * php oil r servernortification:index "arguments"
	 *
	 * @return string
	 */
	public function index($args = NULL)
	{
		echo "\n===========================================";
		echo "\nRunning task [Servernortification:Index]";
		echo "\n-------------------------------------------\n\n";

		/***************************
		 Put in TASK DETAILS HERE
		 **************************/
	}

}
/* End of file tasks/servernortification.php */
