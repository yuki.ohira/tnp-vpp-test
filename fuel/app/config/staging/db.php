<?php
/**
 * Fuel is a fast, lightweight, community driven PHP 5.4+ framework.
 *
 * @package    Fuel
 * @version    1.8.2
 * @author     Fuel Development Team
 * @license    MIT License
 * @copyright  2010 - 2019 Fuel Development Team
 * @link       https://fuelphp.com
 */

/**
 * -----------------------------------------------------------------------------
 *  Global database settings
 * -----------------------------------------------------------------------------
 *
 *  Set database configurations here to override environment specific
 *  configurations
 *
 */

return array(
    'default' => array(
        'type' => 'pdo',
        'connection' => array(
            'dsn' => 'pgsql:host=127.0.0.1;dbname=resttest',
            'username' => 'resttest',
            'password' => 'resttest',
            'persistent' => false,
            'compress' => false,
        ),
        'identifier' => "\"" /* for PostgreSQL */,
        'table_prefix' => '',
        'charset' => NULL,
        'enable_cache' => true,
        'profiling' => false,
    ),
);
